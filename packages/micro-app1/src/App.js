import { RouterProvider, Link, Outlet, json, createBrowserRouter } from 'react-router-dom';
import Page1 from './Page1';
import Page2 from './Page2';
import './App.css';
import NotFound from './NotFound';
import ErrorPage from './ErrorPage';

const appMap = {
  app1: {
    path: '/demo-app01',
    childMap: {
      page1: {
        path: '/page1'
      },
      page2: {
        path: '/page2'
      }
    },
  },
  app2: {
    path: '/demo-app02',
    childMap: {
      page1: {
        path: '/page1'
      },
      page2: {
        path: '/page2'
      }
    },
  },
}

function Layout() {
  const handleClick = (app, page) => {
    window.location.href = appMap[app] ? `${appMap[app]?.path}${appMap[app]?.childMap?.[page]?.path}` : `/${app}/${page}`;
  };

  return (
    <div className='app01-main'>
      <nav className='left'>
        <ul>
          <li>
            <Link to="/demo-app01/page1">app1-page1</Link>
          </li>
          <li>
            <Link to="/demo-app01/page2/normal">app1-page2-正常</Link>
          </li>
          <li>
            <Link to="/demo-app01/page2/error">app1-page2-错误</Link>
          </li>
          <li>
            <Link to="/demo-app01/page3">子应用404</Link>
          </li>
          <li>
            <Link onClick={() => handleClick('app2', 'page1')}>外系统app2</Link>
          </li>
          <li>
            <Link onClick={() => handleClick('app3', 'page1')}>主应用404</Link>
          </li>
        </ul>
      </nav>
      <div className='right'>
        <Outlet />
      </div>
    </div>
  );
}

function App() {
  const router = createBrowserRouter([
    {
      path: "/demo-app01/",
      element: <Layout />,
      children: [
        {
          path: 'page1',
          element: <Page1 />
        },
        {
          path: 'page2/:type',
          element: <Page2 />,
          errorElement: <ErrorPage />,
          loader: ({ params }) => {
            if (params.type === 'error') {
              throw json({ message: 'error 发生啦' });
            }
            return json({ code: 'success', message: '成功数据', data: [{ name: 'horse', age: 18 }] })
          },
        },
        {
          path: '*',
          element: <NotFound />
        },
      ]
    },
  ]);
  return (
    <div className="app01-app">
      <div>这个是app1应用</div>
      <hr />
      <RouterProvider router={router} fallbackElement={<div>你看到此页面说明降级啦</div>} />
    </div>
  );
}

export default App;
