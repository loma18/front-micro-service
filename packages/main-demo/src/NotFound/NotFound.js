import './NotFound.css';
const NotFound = () => (
  <div className="notFound">
    主应用404页面
    <button style={{ marginLeft: 12 }} onClick={() => window.location.href = '/demo-app01'}>返回应用1</button>
  </div>
);

export default NotFound;
