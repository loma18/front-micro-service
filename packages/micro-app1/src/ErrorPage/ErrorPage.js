import { useRouteError } from 'react-router-dom';
import './ErrorPage.css';
const ErrorPage = () => {
  const error = useRouteError();
  console.log(error);
  return (
    <div>
      <div className="errorPage">子应用error页面</div>
      <div>{error?.data?.message}</div>
    </div>
  );
};

export default ErrorPage;
