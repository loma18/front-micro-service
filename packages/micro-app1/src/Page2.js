import { useLoaderData } from 'react-router-dom';

const Page2 = props => {
  const { data } = useLoaderData();
  return (
    <div>
      app1-page2----{JSON.stringify(data)}
    </div>
  );
};

export default Page2;
