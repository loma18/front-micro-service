import { useRoutes, Link, Outlet } from 'react-router-dom';
import Page1 from './Page1';
import Page2 from './Page2';
import './App.css';

const appMap = {
  app1: {
    path: '/demo-app01',
    childMap: {
      page1: {
        path: '/page1'
      },
      page2: {
        path: '/page2'
      }
    },
  },
  app2: {
    path: '/demo-app02',
    childMap: {
      page1: {
        path: '/page1'
      },
      page2: {
        path: '/page2'
      }
    },
  },
}

function Layout() {
  const handleClick = (app, page) => {
    window.location.href = `${appMap[app].path}${appMap[app].childMap[page].path}`;
  };
  return (
    <div className='app02-main'>
      <nav className='left'>
        <ul>
          <li>
            <Link to="/page1">app2-page1</Link>
          </li>
          <li>
            <Link to="/page2">app2-page2</Link>
          </li>
          <li>
            <Link onClick={() => handleClick('app1', 'page1')}>外系统app1</Link>
          </li>
        </ul>
      </nav>
      <div className='right'>
        <Outlet />
      </div>
    </div>
  );
}

function App() {
  const routes = [
    {
      path: "/",
      element: <Layout />,
      children: [
        {
          path: '/page1',
          element: <Page1 />
        },
        {
          path: '/page2',
          element: <Page2 />
        },
        {
          path: '*',
          element: <Page1 />
        },
      ]
    },
  ]
  const element = useRoutes(routes);
  return (
    <div className="app02-app">
      <div>这个是app2应用</div>
      <hr />
      {element}
    </div>
  );
}

export default App;
