import { useEffect } from 'react';
import { useRoutes, Link } from 'react-router-dom';
import './App.css';
import { registerMicroApps, setDefaultMountApp, start } from 'qiankun';
import NotFound from './NotFound/NotFound';

function Layout() {
  return (
    <div className='left'>
      <nav>
        <ul>
          <li>
            <Link to="/demo-app01">app1</Link>
          </li>
          <li>
            <Link to="/demo-app02">app2</Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}

function App() {
  let routes = [
    {
      path: "/demo-app01/*",
      element: <Layout />,
    },
    {
      path: "/demo-app02/*",
      element: <Layout />,
    },
    {
      path: "/*",
      element: <NotFound />,
    },
  ];
  useEffect(() => {
    registerMicroApps([
      {
        name: 'app01',
        entry: '//localhost:3001',
        container: '#container',
        activeRule: '/demo-app01',
      },
      {
        name: 'app02',
        entry: '//localhost:3002',
        container: '#container',
        activeRule: '/demo-app02',
      },
    ]);
    // 启动 qiankun
    start();
    // 设置了默认应用，则不会出现404页面，因为如果找不到符合的应用的路径，会始终跳到默认应用
    // setDefaultMountApp('/demo-app01');
  }, []);

  let element = useRoutes(routes);

  return (
    <div className="main-app">
      <header>主应用header</header>
      <div className='nav'>
        {element}
        <div id="container" className='right' />
      </div>
    </div>
  );
}

export default App;
